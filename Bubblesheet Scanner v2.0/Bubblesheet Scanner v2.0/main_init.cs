﻿using System;
using System.Windows.Forms;
using System.Drawing;

namespace BubblesheetScannerV2_0
{
    partial class main
    {
        void init_Form()
        {
            this.Width = Convert.ToInt32(width * 0.85);
            this.Height = Convert.ToInt32(height * 0.85);
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
        }
        void init_PictureBox()
        {
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox1.Width = Convert.ToInt32(weight * this.Width);
            pictureBox1.Image = Image.FromFile(@"no_image.jpg");
        }
        void init_Data_TextBox()
        {
            data.Size = new Size(Convert.ToInt32((1 - weight) * this.Width), Convert.ToInt32(0.75 * this.Height));
            data.Multiline = true;
            data.ReadOnly = true;
            data.BackColor = Color.Black;
            data.ForeColor = Color.White;
            data.Font = new Font(Font.FontFamily, 15);
            data.TabIndex = 5;
            data.Text = "Waiting for: \r\n \t-python.exe \r\n \t-<script>.py \r\n \t-<image file>\r\n To be Attached.";
        }
        void init_Btns()
        {
            btn_py.TabIndex = 0;
            btn_scr.TabIndex = 1;
            btn_img.TabIndex = 2;
            btn_go.TabIndex = 3;
            btn_expr.TabIndex = 4;
            btn_expr.Enabled = false;
        }
        void init_Labels()
        {
            lbl_py.Size = new Size(Convert.ToInt32(0.2 * this.Width), Convert.ToInt32(0.1 * this.Width));
            lbl_py.MaximumSize = new Size(400, 100);
            lbl_py.AutoSize = true;

            lbl_scr.Size = new Size(Convert.ToInt32(0.2 * this.Width), Convert.ToInt32(0.1 * this.Width));
            lbl_scr.MaximumSize = new Size(400, 100);
            lbl_scr.AutoSize = true;

            lbl_img.Size = new Size(Convert.ToInt32(0.2 * this.Width), Convert.ToInt32(0.1 * this.Width));
            lbl_img.MaximumSize = new Size(400, 100);
            lbl_img.AutoSize = true;
        }
        void init_OpenDialogs()
        {
            di_py.InitialDirectory = @"C:\";
            di_py.RestoreDirectory = true;
            di_py.CheckFileExists = true;
            di_py.CheckPathExists = true;
            di_py.Title = "Browse for python.exe";
            di_py.DefaultExt = "exe";
            di_py.FileName = "python.exe";
            di_py.Multiselect = false;

            di_script.InitialDirectory = desktop;
            di_script.RestoreDirectory = true;
            di_script.CheckFileExists = true;
            di_script.CheckPathExists = true;
            di_script.Title = "Browse for Python Script File .py";
            di_script.DefaultExt = "py";
            di_script.FileName = "*.py";
            di_script.Filter = "Python files (*.py) | *.py";
            di_script.Multiselect = false;

            di_img.InitialDirectory = desktop;
            di_img.RestoreDirectory = true;
            di_img.CheckFileExists = true;
            di_img.CheckPathExists = true;
            di_img.Title = "Browse for Scanned Image";
            di_img.FileName = "";
            di_img.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            di_img.Multiselect = false;
        }
        void init_SaveDialog()
        {
            do_save.Title = "Save Exported Data";
            do_save.InitialDirectory = desktop;
            do_save.RestoreDirectory = true;
            //do_save.CheckFileExists = true;
            do_save.CheckPathExists = true;
            do_save.DefaultExt = "txt";
            do_save.Filter = "txt files (*.txt)|*.txt";
            do_save.FileName = img_filename + ".txt";
        }
    }
}
