﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.IO;

namespace BubblesheetScannerV2_0
{
    partial  class main
    {
        private void btn_py_Click(object sender, EventArgs e)
        {
            if (di_py.ShowDialog() == DialogResult.OK)
            {
                path_python = di_py.FileName;
                lbl_py.Text = "python.exe is Attached";
            }
        }
        private void btn_scr_Click(object sender, EventArgs e)
        {
            if (di_script.ShowDialog() == DialogResult.OK)
            {
                path_script = di_script.FileName;
                script_filename = Path.GetFileName(path_script);
                lbl_scr.Text = "Script file " + script_filename + " is Attached";
            }
        }
        private void btn_img_Click(object sender, EventArgs e)
        {
            if (di_img.ShowDialog() == DialogResult.OK)
            {
                path_img = di_img.FileName;
                img_filename_ext = Path.GetFileName(path_img);
                img_filename = Path.GetFileNameWithoutExtension(path_img);
                img_extension = Path.GetExtension(path_img);
                pictureBox1.Image = Image.FromFile(path_img);
                this.Text = "Bubblesheet Scanner for " + img_filename_ext;
                lbl_img.Text = "Image file " + img_filename + " is Attached";
            }
        }
        private void btn_go_Click(object sender, EventArgs e)
        {
            data.Text = String.Empty;
            if (go_empty_check())
            {
                MessageBox.Show("Plase Make Sure to Attach all Files", "ERROR!!!");
            }
            else
            {
                string proc_error="", proc_result="";
                data.Text += "Executing Python Script file: " + script_filename + "\r\n" 
                    + "For Image file: " + img_filename_ext + "\r\n" + "\r\n";
                //script_call obj = script_call.runScript(path_python, path_script, path_img);
                script_call.runScript(path_python, path_script, path_img, ref proc_result, ref proc_error);
                data.Text += proc_result + "\r\n";
                data.Text += proc_result + "\r\n";
                if (proc_error == "")
                    data.Text += "Script Executed Successfully!\r\n";
                else if (proc_error != "")
                    data.Text += "Error: " + proc_error + "\r\n";
                btn_expr.Enabled = true;
                result += proc_result;
            }
        }
        private void btn_expr_Click(object sender, EventArgs e)
        {
            if (go_empty_check())
            {
                MessageBox.Show("Plase Make Sure to Attach all Files", "ERROR!!!");
            }
            else
            {
                init_SaveDialog();
                if (do_save.ShowDialog() == DialogResult.OK)
                {
                    StreamWriter sw = new StreamWriter(do_save.FileName);
                    sw.WriteLine(result);
                    sw.Flush();
                    sw.Close();
                    MessageBox.Show("File was exported to "+do_save.FileName + " Successfully");
                }
            }
        }
        private void embd_script_CheckedChanged(object sender, EventArgs e)
        {
            if (embd_script.Checked == true)
            {
                btn_scr.Enabled = false;
                path_script = @"bubbleScan.py";
                script_filename = Path.GetFileName(path_script);
                lbl_scr.Text = @"/bubbleScan.py";
            }
            if (embd_script.Checked == false)
            {
                btn_scr.Enabled = true;
                lbl_scr.Text = "Script Path";
            }
        }
    }
}
