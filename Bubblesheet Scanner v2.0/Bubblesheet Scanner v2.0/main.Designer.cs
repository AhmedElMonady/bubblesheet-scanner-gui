﻿namespace BubblesheetScannerV2_0
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.embd_script = new System.Windows.Forms.CheckBox();
            this.btn_img = new System.Windows.Forms.Button();
            this.lbl_img = new System.Windows.Forms.Label();
            this.btn_py = new System.Windows.Forms.Button();
            this.btn_go = new System.Windows.Forms.Button();
            this.btn_scr = new System.Windows.Forms.Button();
            this.btn_expr = new System.Windows.Forms.Button();
            this.lbl_scr = new System.Windows.Forms.Label();
            this.lbl_py = new System.Windows.Forms.Label();
            this.data = new System.Windows.Forms.TextBox();
            this.di_py = new System.Windows.Forms.OpenFileDialog();
            this.di_script = new System.Windows.Forms.OpenFileDialog();
            this.di_img = new System.Windows.Forms.OpenFileDialog();
            this.do_save = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox1.Location = new System.Drawing.Point(574, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(317, 443);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.embd_script);
            this.panel1.Controls.Add(this.btn_img);
            this.panel1.Controls.Add(this.lbl_img);
            this.panel1.Controls.Add(this.btn_py);
            this.panel1.Controls.Add(this.btn_go);
            this.panel1.Controls.Add(this.btn_scr);
            this.panel1.Controls.Add(this.btn_expr);
            this.panel1.Controls.Add(this.lbl_scr);
            this.panel1.Controls.Add(this.lbl_py);
            this.panel1.Controls.Add(this.data);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(574, 443);
            this.panel1.TabIndex = 1;
            // 
            // embd_script
            // 
            this.embd_script.AutoSize = true;
            this.embd_script.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.embd_script.Location = new System.Drawing.Point(376, 73);
            this.embd_script.Name = "embd_script";
            this.embd_script.Size = new System.Drawing.Size(198, 24);
            this.embd_script.TabIndex = 10;
            this.embd_script.Text = "Use embedded script";
            this.embd_script.UseVisualStyleBackColor = true;
            this.embd_script.CheckedChanged += new System.EventHandler(this.embd_script_CheckedChanged);
            // 
            // btn_img
            // 
            this.btn_img.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btn_img.Location = new System.Drawing.Point(213, 12);
            this.btn_img.Name = "btn_img";
            this.btn_img.Size = new System.Drawing.Size(91, 34);
            this.btn_img.TabIndex = 9;
            this.btn_img.Text = "Image";
            this.btn_img.UseVisualStyleBackColor = true;
            this.btn_img.Click += new System.EventHandler(this.btn_img_Click);
            // 
            // lbl_img
            // 
            this.lbl_img.AutoSize = true;
            this.lbl_img.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_img.Location = new System.Drawing.Point(19, 78);
            this.lbl_img.Name = "lbl_img";
            this.lbl_img.Size = new System.Drawing.Size(76, 16);
            this.lbl_img.TabIndex = 8;
            this.lbl_img.Text = "Image Path";
            // 
            // btn_py
            // 
            this.btn_py.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btn_py.Location = new System.Drawing.Point(12, 12);
            this.btn_py.Name = "btn_py";
            this.btn_py.Size = new System.Drawing.Size(91, 34);
            this.btn_py.TabIndex = 0;
            this.btn_py.Text = "python.exe";
            this.btn_py.UseVisualStyleBackColor = true;
            this.btn_py.Click += new System.EventHandler(this.btn_py_Click);
            // 
            // btn_go
            // 
            this.btn_go.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btn_go.Location = new System.Drawing.Point(310, 12);
            this.btn_go.Name = "btn_go";
            this.btn_go.Size = new System.Drawing.Size(91, 34);
            this.btn_go.TabIndex = 2;
            this.btn_go.Text = "Go";
            this.btn_go.UseVisualStyleBackColor = true;
            this.btn_go.Click += new System.EventHandler(this.btn_go_Click);
            // 
            // btn_scr
            // 
            this.btn_scr.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btn_scr.Location = new System.Drawing.Point(109, 12);
            this.btn_scr.Name = "btn_scr";
            this.btn_scr.Size = new System.Drawing.Size(98, 34);
            this.btn_scr.TabIndex = 1;
            this.btn_scr.Text = "Script";
            this.btn_scr.UseVisualStyleBackColor = true;
            this.btn_scr.Click += new System.EventHandler(this.btn_scr_Click);
            // 
            // btn_expr
            // 
            this.btn_expr.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btn_expr.Location = new System.Drawing.Point(407, 12);
            this.btn_expr.Name = "btn_expr";
            this.btn_expr.Size = new System.Drawing.Size(98, 34);
            this.btn_expr.TabIndex = 3;
            this.btn_expr.Text = "Export";
            this.btn_expr.UseVisualStyleBackColor = true;
            this.btn_expr.Click += new System.EventHandler(this.btn_expr_Click);
            // 
            // lbl_scr
            // 
            this.lbl_scr.AutoSize = true;
            this.lbl_scr.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_scr.Location = new System.Drawing.Point(257, 58);
            this.lbl_scr.Name = "lbl_scr";
            this.lbl_scr.Size = new System.Drawing.Size(72, 16);
            this.lbl_scr.TabIndex = 2;
            this.lbl_scr.Text = "Script Path";
            // 
            // lbl_py
            // 
            this.lbl_py.AutoSize = true;
            this.lbl_py.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_py.Location = new System.Drawing.Point(19, 58);
            this.lbl_py.Name = "lbl_py";
            this.lbl_py.Size = new System.Drawing.Size(103, 16);
            this.lbl_py.TabIndex = 1;
            this.lbl_py.Text = "python.exe Path";
            // 
            // data
            // 
            this.data.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.data.Location = new System.Drawing.Point(0, 97);
            this.data.Multiline = true;
            this.data.Name = "data";
            this.data.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.data.Size = new System.Drawing.Size(574, 346);
            this.data.TabIndex = 0;
            // 
            // di_py
            // 
            this.di_py.FileName = "openFileDialog1";
            // 
            // di_script
            // 
            this.di_script.FileName = "openFileDialog1";
            // 
            // di_img
            // 
            this.di_img.FileName = "openFileDialog1";
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(891, 443);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox data;
        private System.Windows.Forms.Label lbl_scr;
        private System.Windows.Forms.Label lbl_py;
        private System.Windows.Forms.Button btn_expr;
        private System.Windows.Forms.Button btn_scr;
        private System.Windows.Forms.Button btn_go;
        private System.Windows.Forms.Button btn_py;
        private System.Windows.Forms.Label lbl_img;
        private System.Windows.Forms.OpenFileDialog di_py;
        private System.Windows.Forms.OpenFileDialog di_script;
        private System.Windows.Forms.OpenFileDialog di_img;
        private System.Windows.Forms.SaveFileDialog do_save;
        private System.Windows.Forms.Button btn_img;
        private System.Windows.Forms.CheckBox embd_script;
    }
}

