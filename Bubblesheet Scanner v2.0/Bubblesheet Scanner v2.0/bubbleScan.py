import cv2
from imutils import contours
import imutils
import sys

def runScript(path):
    #Return Variable
    ret_answer = ""
    
    # Dictionaries carrying each bubble's index and their corresponding choice or answer in bubble sheet

    # survey questions section
    bottom_keys = {0:"Strongly Agree", 1:"Agree", 2:"Neutral", 3:"Disagree", 4:"Strongly Disagree"}

    # gender section 
    gender_keys = {0:"Male", 1:"Female"}

    # semester section 
    semester_keys = {0:"Fall", 1:"Spring", 2:"Summer"}

    # program section (divided into to lines)
    program_keys_1 = {0:"MCTA", 1:"ENVER", 2:"BLDG", 3:"CESS", 4:"ERGY", 5:"COMM", 6:"MANF"}
    program_keys_2 =  {0:"LAAR", 1:"MATL", 2:"CISE", 3:"HAUD"}

    # opening a text file to write reults in 
    #answers_file = open("Student_1.txt", "w+")

    ######################################################
    # OBTAINING IMAGE AND MAKING IT READY FOR PROCESSING
    ######################################################

    # read image 
    img = cv2.imread(path)

    # convert to grayscale 
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # not each pixel in the gray image 
    gray = cv2.bitwise_not(gray)

    # apply threshold to the image (THRESH_BINARY -> transforms the image to a binary image)
    # (THRESH_OTSU -> to use the optimal threshold) 
    thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

    coords = np.column_stack(np.where(thresh > 0))

    angle = cv2.minAreaRect(coords)[-1]

    ################################################################
    # DETECTING ANY ROTATION IN THE OBTAINED IMAGE AND DEROTATE IT
    ################################################################

    # the `cv2.minAreaRect` function returns values in the
    # range [-90, 0); as the rectangle rotates clockwise the
    # returned angle trends to 0 -- in this special case we
    # need to add 90 degrees to the angle
    if angle < -45:
        angle = -(90 + angle)

    # otherwise, just take the inverse of the angle to make
    # it positive
    else:
        angle = -angle

    # rotate the image to deskew it
    (h, w) = img.shape[:2]
    center = (w // 2, h // 2)
    M = cv2.getRotationMatrix2D(center, angle, 1.0)
    img = cv2.warpAffine(img, M, (w, h), flags=cv2.INTER_CUBIC, borderMode=cv2.BORDER_REPLICATE)


    # the sheet was divided into 2 sections 

    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Detecting the place of the bubbles in the sheet using the hough Algorithm
    hough_circles = cv2.HoughCircles(img, cv2.HOUGH_GRADIENT, 1, 20, param1=50, param2=30, minRadius=10, maxRadius=20)
    circles = np.uint16(np.around(hough_circles))

    for i in circles[0, :]:
        cv2.circle(img, (i[0], i[1]), i[2], (0, 255, 0), 2)
        
    thresh = cv2.threshold(img, 20, 255, cv2.THRESH_BINARY_INV)[1]

    # top_part -> part containing (Gender, Semester, Program Questions)
    top_part = thresh[250:525, 330:1450]

    # bottom_part -> part conataining the main servey questions
    bottom_part = thresh[950:2100, 1100:1550]

    # find the bubbles 
    cnts = cv2.findContours(top_part.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # grab detected contours
    cnts = imutils.grab_contours(cnts)

    # sort detected contours from top to bottom to be placed as they are exactly in the sheet
    cnts = contours.sort_contours(cnts, method="top-to-bottom")[0]

    ############################################
    # SLICING THE DETECTED CONTOURS TO SECTIONS
    ############################################

    # Gender Contours
    gender_cnts = cnts[0:2]

    # Semester Contours
    semester_cnts = cnts[2:5]

    # program Contours (Divided into 2 as they appear in 2 lines)
    program_cnts_1 = cnts[5:12]
    program_cnts_2 = cnts[12:16]

    # listing all sections in one tuple
    top_part_cnts = [gender_cnts, semester_cnts, program_cnts_1, program_cnts_2]
    bubbs = []

    ############################################
    # DETECTING ANSWERS FOR TOP PART
    ############################################

    for k in range(len(top_part_cnts)):
        for (q, i) in enumerate(np.arange(0, 1, len(top_part_cnts[k]))):
            # sort the contours for the current question from
            # left to right, then initialize the index of the
            # bubbled answer
            question_cnt = contours.sort_contours(top_part_cnts[k][i:i + len(top_part_cnts[k])])[0]
            bubbled = None

            # loop over the sorted contours
            for (j, c) in enumerate(question_cnt):
                # construct a mask that reveals only the current
                # "bubble" for the question
                mask = np.zeros(top_part.shape, dtype="uint8")
                cv2.drawContours(mask, [c], -1, 255, -1)

                # apply the mask to the thresholded image, then
                # count the number of non-zero pixels in the
                # bubble area
                mask = cv2.bitwise_and(top_part, top_part, mask=mask)
                total = cv2.countNonZero(mask)

                # if the current total has a larger number of total
                # non-zero pixels, then we are examining the currently
                # bubbled-in answer
                if bubbled is None or total > bubbled[0]:
                    bubbled = (total, j)
            bubbs.append(bubbled)
            
    # If there was no answer and no bubbles were shaded in a section write NONE in file 
    # Otherwise write the cosen answer in file

    if bubbs[0][0] > 450 :
        ret_answer = ret_answer + "Gender: " + gender_keys[bubbs[0][1]] + "\r\n"
    else:
        ret_answer = ret_answer + "Gender: None\r\n"

    if bubbs[1][0] > 450 :
        ret_answer = ret_answer + "Semester: " + semester_keys[bubbs[1][1]] + "\r\n"
    else:
        ret_answer = ret_answer + "Semester: None\r\n"

    program_bubble = None
    program_name = None
    if bubbs[2][0] > bubbs[3][0]:
        program_bubble = bubbs[2]
        program_name = program_keys_1[program_bubble[1]]
    else:
        program_bubble = bubbs[3]
        program_name = program_keys_2[program_bubble[1]]
    if program_bubble[0] > 450 :
        ret_answer = ret_answer + "Program: " + program_name + "\r\n"
    else:
        ret_answer = ret_answer + "Program: None\r\n"
        
    ############################################
    # DETECTING ANSWERS FOR BOTTOM PART
    ############################################

    cnts = cv2.findContours(bottom_part.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    cnts = contours.sort_contours(cnts, method="top-to-bottom")[0]

    for (q, i) in enumerate(np.arange(0, len(cnts), 5)):
        # sort the contours for the current question from
        # left to right, then initialize the index of the
        # bubbled answer
        question_cnt = contours.sort_contours(cnts[i:i + 5])[0]
        bubbled = None

        # loop over the sorted contours
        for (j, c) in enumerate(question_cnt):
            # construct a mask that reveals only the current
            # "bubble" for the question
            mask = np.zeros(bottom_part.shape, dtype="uint8")
            cv2.drawContours(mask, [c], -1, 255, -1)

            # apply the mask to the thresholded image, then
            # count the number of non-zero pixels in the
            # bubble area
            mask = cv2.bitwise_and(bottom_part, bottom_part, mask=mask)
            total = cv2.countNonZero(mask)

            # if the current total has a larger number of total
            # non-zero pixels, then we are examining the currently
            # bubbled-in answer
            if bubbled is None or total > bubbled[0]:
                bubbled = (total, j)

        if bubbled[0] > 450:
            ret_answer = ret_answer + str(q+1) + "." + bottom_keys[bubbled[1]] + "\r\n"
        else:
            ret_answer = ret_answer + str(q + 1) + "." + "No Answer" + "\r\n"

    print(ret_answer)
    
if __name__ == "__main__":
    path = sys.argv[1]
    runScript(path)