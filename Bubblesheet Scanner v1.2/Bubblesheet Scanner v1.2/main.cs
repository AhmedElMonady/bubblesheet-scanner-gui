﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;

namespace BubblesheetScannerV1_2
{
    public partial class main : Form
    {
        public static float width = Screen.PrimaryScreen.WorkingArea.Width;
        public static float height = Screen.PrimaryScreen.WorkingArea.Height;

        public static float weight = 0.45f;

        static string desktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

        static string path_img="", img_filename, img_extension,img_filename_ext;
        static string path_python="", path_script="",script_filename;

        static string result = "";

        public main()
        {
            InitializeComponent();
            init_Form();
            init_PictureBox();
            init_Data_TextBox();
            init_Btns(); 
            init_Labels();
            init_OpenDialogs();

            this.SizeChanged += Form1_SizeChanged;
            this.Text = "Bubblesheet Scanner v1.2";
        }
        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            init_Form();
            init_PictureBox();
            init_Data_TextBox();
            init_Btns();
            init_Labels();
        }
        bool go_empty_check()
        {
            if (path_python == "" || path_script == "" || path_img == "")
                return true;
            return false;
        }
    }
}
