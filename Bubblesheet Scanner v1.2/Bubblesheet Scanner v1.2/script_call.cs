﻿using System.Diagnostics;

namespace BubblesheetScannerV1_2
{
    class script_call
    {
        /// <summary>
        /// runScript runs a Python script to obtain results from the script scanning a bubblesheet and returns its answers.
        /// </summary>
        /// <param name="python_path">Path for python.exe file.</param>
        /// <param name="script_path">Path for runnable python script file.</param>
        /// <param name="img_path">Path for image to be analysed.</param>
        /// <param name="result">Result variable  for the execution result to be passed to by reference.</param>
        /// <param name="error_msg">Error Message variable  for the execution error to be passed to by reference if encountered.</param>
        public static void runScript
            (string python_path,string script_path,string img_path,ref string result, ref string error_msg)
        {
            var psi = new ProcessStartInfo();
            psi.FileName = python_path;
            var script = script_path;
            psi.Arguments = script + " " + img_path;

            psi.UseShellExecute = false;
            psi.CreateNoWindow = true;
            psi.RedirectStandardOutput = true;
            psi.RedirectStandardError = true;

            using (var process = Process.Start(psi))
            {
                result = process.StandardOutput.ReadToEnd();
                error_msg = process.StandardError.ReadToEnd();
            }

        } 
    }
}
